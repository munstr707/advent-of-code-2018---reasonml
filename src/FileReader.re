let dirname: option(string) = [%bs.node __dirname];

let fileContentByLine = relativeFilePath => {
  let fileContent =
    switch dirname {
    | None => "no content found"
    | Some(string) =>
      Node.Path.join([|string, relativeFilePath|])
      |> Node.Fs.readFileAsUtf8Sync
    };
  fileContent |> Js.String.split("\n");
};