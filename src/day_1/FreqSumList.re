let () =
  "./input.txt"
  |> FileReader.fileContentByLine
  |> Array.map(frequency =>
       (
         String.contains(frequency, '+') ?
           String.sub(frequency, 1, String.length(frequency) - 1) : frequency
       )
       |> int_of_string
     )
  |> Array.fold_left((sum, frequency) => sum + frequency, 0)
  |> Js.log;