/*
   frequency will be reached multiple times
     continue cycling through the list until a redundant number is found

   naive solution
     dictionary cache of values that is assessed until a solution is found
     prefer set for only allowing unique values? (efficient ds for calculating)

   use recursion to conitinually iterate with context
 */